package ExtraMystcraftSymbols.symbols;

import java.util.List;

import net.minecraft.src.BiomeGenBase;
import xcompwiz.mystcraft.api.AgeSymbol;
import xcompwiz.mystcraft.api.IAgeController;
import xcompwiz.mystcraft.api.IBiomeController;

public class symbol_BiomeControllerIsolated extends AgeSymbol
{
  public void instantiate(IAgeController controller)
  {
    controller.registerInterface(this, new BiomeController());
  }

  public String identifier()
  {
    return "Isolated Biome";
  }

  public AgeSymbol.Category getCategory() {
    return AgeSymbol.Category.BiomeController;
  }

  private class BiomeController implements IBiomeController
  {
    private List biomes;

    public BiomeController() {
    }

    public int getMinBiomeCount() {
      return 3;
    }

    public int getMaxBiomeCount()
    {
      return 3;
    }

    public void setBiomes(List biomes)
    {
      this.biomes = biomes;
    }

    /**
     * Gets the list of valid biomes for the player to spawn in.
     */
    public List a()
    {
      return this.biomes;
    }

    public float[] a(float[] af, int x, int z, int k, int l)
    {
      if ((af == null) || (af.length < k * l))
      {
        af = new float[k * l];
      }
      for (int i1 = 0; i1 < k * l; i1++)
      {
        float f = a(x + i1 % k, z + i1 / k).rainfall;
        if (f > 1.0F)
        {
          f = 1.0F;
        }
        af[i1] = f;
      }

      return af;
    }

    public float[] b(float[] af, int x, int z, int k, int l)
    {
      if ((af == null) || (af.length < k * l))
      {
        af = new float[k * l];
      }
      for (int i1 = 0; i1 < k * l; i1++)
      {
        float f = a(x + i1 % k, z + i1 / k).temperature;
        if (f > 1.0F)
        {
          f = 1.0F;
        }
        af[i1] = f;
      }

      return af;
    }

    /**
     * Returns the BiomeGenBase related to the x, z position on the world.
     */
    public BiomeGenBase a(int x, int z)
    {
    	if(x < 20 && x > -20 && z < 20 && z > -20) {
    		return (BiomeGenBase)this.biomes.get(0);
    	} else if (x < 28 && x > -28 && z < 28 && z > -28) {
    		return (BiomeGenBase)this.biomes.get(1);
    	} else {
    		return (BiomeGenBase)this.biomes.get(2);
    	}
//    	return (BiomeGenBase)this.biomes.get(0);
//      return (BiomeGenBase)this.biomes.get((i >> 4) + (j >> 4) & 0x1);
    }

    public BiomeGenBase[] a(BiomeGenBase[] abiomegenbase, int i, int j, int k, int l)
    {
      if ((abiomegenbase == null) || (abiomegenbase.length < k * l))
      {
        abiomegenbase = new BiomeGenBase[k * l];
      }

      for (int var7 = 0; var7 < k * l; var7++)
      {
        abiomegenbase[var7] = a(i + var7 % k, j + var7 / k);
      }

      return abiomegenbase;
    }

    public BiomeGenBase[] a(BiomeGenBase[] abiomegenbase, int i, int j, int k, int l, boolean flag)
    {
      if ((abiomegenbase == null) || (abiomegenbase.length < k * l))
      {
        abiomegenbase = new BiomeGenBase[k * l];
      }
      if ((flag) && (k == 16) && (l == 16) && ((i & 0xF) == 0) && ((j & 0xF) == 0))
      {
        return createBiomeArray(abiomegenbase, i, j, k, l);
      }
      for (int i1 = 0; i1 < k * l; i1++)
      {
        abiomegenbase[i1] = a(i + i1 % k, j + i1 / k);
      }

      return abiomegenbase;
    }

    private BiomeGenBase[] createBiomeArray(BiomeGenBase[] abiomegenbase, int i, int j, int k, int l)
    {
      if ((abiomegenbase == null) || (abiomegenbase.length < k * l))
      {
        abiomegenbase = new BiomeGenBase[k * l];
      }
      for (int i1 = 0; i1 < k * l; i1++) {
        abiomegenbase[i1] = a(i + i1 % k, j + i1 / k);
      }
      return abiomegenbase;
    }

    public float a(float f, int y)
    {
      return f;
    }

    public void b()
    {
    }
  }
}