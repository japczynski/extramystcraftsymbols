package ExtraMystcraftSymbols.symbols;

import net.minecraft.src.Block;
import xcompwiz.mystcraft.api.AgeSymbol;
import xcompwiz.mystcraft.api.AgeSymbol.Category;
import xcompwiz.mystcraft.api.IAgeController;
import xcompwiz.mystcraft.api.ITerrainGenerator;

public class symbol_TerrainGenIsland extends AgeSymbol
{
    public void instantiate(IAgeController var1)
    {
        var1.registerInterface(this, (ITerrainGenerator)(new TerrainGenerator(this, var1)));
    }

    public String identifier()
    {
        return "Island";
    }

    public AgeSymbol.Category getCategory()
    {
        return AgeSymbol.Category.TerrainGen;
    }
    
    class TerrainGenerator implements ITerrainGenerator
    {
        private IAgeController controller;

        final symbol_TerrainGenIsland parent;

        public TerrainGenerator(symbol_TerrainGenIsland var1, IAgeController var2)
        {
            this.parent = var1;
            this.controller = var2;
        }

        public void generateTerrain(int chunkX, int chunkZ, byte[] var3)
        {
            int groundLevel = this.controller.getAverageGroundLevel();
            int seaLevel = this.controller.getSeaLevel();
            int height = var3.length / 256;

            for (int x = 0; x < 16; x++)
            {
                for (int z = 0; z < 16; z++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        byte var10 = 0;
                        
                        if (y == 0)
                        {
                            var10 = (byte)Block.bedrock.blockID;
                        }
                        else if (y < groundLevel)
                        {
                            var10 = (byte)Block.stone.blockID;
                        }
                        else if (y <= seaLevel)
                        {
                            var10 = (byte)Block.waterStill.blockID;
                        }
                        
                        if(chunkX == 0 && chunkZ == 0)
                        {
                        	if (x == 0 || z == 0) {
                        		var10 = (byte)Block.obsidian.blockID;
                        	} else {
                        		var10 = (byte)Block.bedrock.blockID;
                        	}
                        }

                        var3[x << 11 | z << 7 | y] = var10;
                    }
                }
            }
        }
    }
}