package ExtraMystcraftSymbols;

import xcompwiz.mystcraft.api.APICallHandler;
import ExtraMystcraftSymbols.symbols.symbol_BiomeControllerIsolated;
import ExtraMystcraftSymbols.symbols.symbol_TerrainGenIsland;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;

@Mod(modid="mod_mystcraftSymbols", version="0.1", name="mystcraftSymbols", useMetadata=true)
public class mod_mystcraftSymbols {
	@Mod.Init
	public void init(FMLInitializationEvent event)
	{
		APICallHandler.registerSymbol(new symbol_BiomeControllerIsolated());
		APICallHandler.registerSymbol(new symbol_TerrainGenIsland());
	}

}
